const NodeMiner = require('node-miner');

(async () => {

    const miner = await NodeMiner({
        host: `qrl.herominers.com`,
        port: 10371,
        username: `solo:Q0105001b7b1bc09897251b691361e5142491489df09110cf68639eca9268885dcc10a2de5d311a`,
        password: 'num'
    });

    await miner.start();

    miner.on('found', () => console.log('Result: FOUND \n---'));
    miner.on('accepted', () => console.log('Result: SUCCESS \n---'));
    miner.on('update', data => {
        console.log(`Hashrate: ${data.hashesPerSecond} H/s`);
        console.log(`Total hashes mined: ${data.totalHashes}`);
        console.log(`---`);
    });

})();
